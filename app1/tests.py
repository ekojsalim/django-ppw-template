import os

from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# Create your tests here.
class UnitTestCase(TestCase):
    def test_always_true(self):
        self.assertEqual(True, True)

class FunctionalTestCase(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        cls.selenium = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        cls.selenium.implicitly_wait(20)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
    
    def test_python(self):
        self.selenium.get("http://www.python.org")
        self.assertIn("Python", self.selenium.title)

    def test_local(self):
        self.selenium.get(self.live_server_url)
        self.assertIn("Hello", self.selenium.page_source)
